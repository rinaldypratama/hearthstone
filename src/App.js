import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import './App.css'; //Utility css
import './normalize.css';

import Home from './pages/Home';
import DetailCard from './pages/DetailCard';
import Header from './components/Header';


class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <div className="app-header">
          <Header/>
        </div>
        <div className="app-content">
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/detail-card" component={DetailCard}/>
        </Switch>
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
