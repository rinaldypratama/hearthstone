import axios from 'axios';
import store from '../store';

import {  GET_ERRORS, GET_CARD, CARD_LOADING, SET_DETAILCARD, SET_VIEW, SHOW_CARD, PICK_CARDSET  } from '../actions/types';

// Get Card from api
export const getCard = () => dispatch => {
	dispatch(setCardLoading());
		axios
			.get(`/cards?collectible=1`)
			.then(res => {
        const payload = Object.entries(res.data).filter(key => key[1].length !== 0)
                      .reduce((obj, key) => {
                              obj[key[0]] = key[1];
                              return obj;
                            }, {});
        dispatch({
          type: GET_CARD,
          payload
        });
        dispatch(pickCardset(Object.keys(payload)[0]));
      })
			.catch(err =>
				dispatch({
					type: GET_ERRORS,
					payload: err.response
				})
			);
};

//Pick Card set
export const pickCardset = (data) => dispatch => {
  if(window.innerWidth >= 800){
    dispatch({
      type:SET_VIEW,
      payload:{add:15, view: 15}
    });
  }
  else{
    dispatch({
      type:SET_VIEW,
      payload:{add:10, view: 10}
    });
  }
  dispatch({
    type:PICK_CARDSET,
    payload:data
  });
  dispatch(showCard(store.getState().card.cards, data));
};

//Set Card set
export const showCard = (cards, filterCard) => dispatch => {
  const payload = cards[filterCard];
  dispatch({
    type:SHOW_CARD,
    payload
  });
};

export const setDetailCard = (card, history) => dispatch => {
  dispatch({
    type:SET_DETAILCARD,
    payload:card
  });
  history.push('/detail-card');
};


export const setCardLoading = () => {
  return {
    type: CARD_LOADING
  };
};
