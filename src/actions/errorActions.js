import { GET_ERRORS, CLEAR_ERRORS } from './types';

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const getError = (payload) => {
	return{
		type: GET_ERRORS,
      payload: payload
	}
}