export const GET_CARD = 'GET_CARD';
export const PICK_CARDSET = 'PICK_CARDSET';
export const SHOW_CARD = 'SHOW_CARD';
export const SET_DETAILCARD = 'SET_CARD';
export const ADD_VIEW = 'ADD_VIEW';
export const SET_VIEW = 'SET_VIEW';
export const CARD_LOADING = 'CARD_LOADING';
export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';


