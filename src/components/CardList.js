import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { getCard, setDetailCard } from '../actions/cardActions';
import { addView } from '../actions/commonActions';
import './CardList.css';

import CardWrapper from './CardWrapper';
import Spinner from '../utils/Spinner';


import Icon from '../assets/icon-mobile.png';
import MoreWeb from '../assets/button-web.png';
import MoreMobile from '../assets/button-mobile.png';

class CardList extends Component {
	constructor(props) {
    super(props);
    this.state = {
      items: [],
      view: null,
      refresh:true,
    };
	   this.clickHandler = this.clickHandler.bind(this);
	   this.loadMore = this.loadMore.bind(this);
 	}

	componentDidMount(){
		const { cards } =this.props.card;
		if(Object.keys(cards).length === 0 )this.props.getCard();
		this.setState({
			items: this.props.card.cardShow,
			refresh: false,
			view: this.props.common.view
		})
	}

	componentWillReceiveProps(nextProps) {
   	if(nextProps.common.view){
   		this.setState({
				view: nextProps.common.view
			})
   	}
 		if (nextProps.card.cardShow && this.state.items.length === 0) {
    	this.setState({ 
  			items: nextProps.card.cardShow,
  			refresh: false
  	  })
   	}
   	if (nextProps.card.cardShow && this.state.items.length !== 0) {
    	this.setState({ 
  			items: nextProps.card.cardShow,
  			refresh: true
  	  })
  	  setTimeout(() => {
			  this.setState({refresh: false});
			}, 500)
   	}
	}

  loadMore() {
  	const { view } = this.state;
  	const { addCard } = this.props.common;
  	this.props.addView(view, addCard);
  }

 	clickHandler(data){
 		this.props.setDetailCard(data, this.props.history);
 	}

	render() {
		const { cardSet } = this.props.card;
		const { items, refresh, view } = this.state;
		return (
			<div className="wrapper">
				<div className="title-card">
					<h1>Cards</h1>
					<hr/>
					<h1>{cardSet}</h1>
				</div>
			{
				(!this.props.card.loading && !refresh) ?
					<React.Fragment>
						<div className="cardlist-container">
							{
								items.slice(0,view).map((x, index) =>(
									<div className="card-item" key={index} onClick={() => this.clickHandler(x)}>
										<CardWrapper detail="false" image={x.img}/>
										<div className="card-name">
											<img src={Icon} alt="Icon Hearthstone"/>
											<p>{x.name}</p>
										</div>
									</div>
								))
							}
						</div>
						{
							(items.length >= view) &&
						<div className="more-wrapper" >
							<img src={MoreWeb} srcSet={`${MoreMobile} 320w, ${MoreWeb} 1366w`} alt="More" onClick={this.loadMore}/>
						</div>
						}
					</React.Fragment> :
					 <Spinner/>
			}
				<div className="cardlist-footer">
					<h1>Play your way:</h1>
					<p>Find your perfect match in casual, ranked, and arena games, participate incrazy Tavern Brawls, or practice against devious AI opponents and hone your skills in Hearthstone’s adventure mode.</p>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  	card:state.card,
  	common:state.common,
    error:state.error
});

export default withRouter(connect(mapStateToProps, { getCard, setDetailCard, addView }) (CardList));