import React, { Component } from 'react';
import classnames from 'classnames';
import './CardWrapper.css';

import Cardback from '../assets/cardback.png';

class CardWrapper extends Component {
	render() {
		const { detail, image } = this.props;
		return (
			<div className="card-wrapper">
				<img className={classnames("front-card", {"front-mini": detail.toLowerCase() === 'false'})} src={image} alt="Card"/>
				<img className={classnames("back-card", {"back-mini": detail.toLowerCase() === 'false'})} src={Cardback} alt="cardback"/>
				<div className={classnames("shadow-card", {"shadow-mini": detail.toLowerCase() === 'false'})}></div>
			</div>
		);
	}
}

export default CardWrapper;