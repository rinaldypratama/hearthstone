import React, { Component } from 'react';
import './Footer.css';

import IconWeb from "../assets/icon-web.png";
import IconMobile from "../assets/icon-mobile.png";
import LogoFB from "../assets/logo-facebook.png";
import LogoTwitter from "../assets/logo-twitter.png";
import LogoIG from "../assets/logo-instagram.png";
import LogoUTube from "../assets/logo-youtube.png";
import LogoGP from "../assets/logo-googleplus.png";
import LogoReddit from "../assets/logo-reddit.png";

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<div className="wrapper">
					<img className="icon-footer" src={IconWeb} srcSet={`${IconMobile} 320w, ${IconWeb} 1366w`} alt=""/>
					<div className="footer-socialmedia">
					<p>Stay Connected</p>
					<div className="footer-link">
						<div className="social-wrapper"><a href="http://facebook.com"><img className="logo-spc" src={LogoFB} alt="Facebook"/></a></div>
						<div className="social-wrapper"><a href="http://twitter.com"><img src={LogoTwitter} alt="Twitter"/></a></div>
						<div className="social-wrapper"><a href="http://instagram.com"><img src={LogoIG} alt="Instagram"/></a></div>
						<div className="social-wrapper"><a href="http://youtube.com"><img src={LogoUTube} alt="Youtube"/></a></div>
						<div className="social-wrapper"><a href="http://plus.google.com/"><img src={LogoGP} alt="GooglePlus"/></a></div>
						<div className="social-wrapper"><a href="http://reddit.com"><img src={LogoReddit} alt="Reddit"/></a></div>
					
					</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Footer;