import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { pickCardset, setCardLoading } from '../actions/cardActions';
import './Header.css';

import SearchBar from "./SearchBar";

import LogoWeb from '../assets/logo-mini-web.png';
import LogoMobile from '../assets/logo-mini-mobile.png';

class Header extends Component {
	constructor(props) {
	  super(props);
	  this.state = {
      show: false,
      navbarShow: false
    };
	  this.handleMenu = this.handleMenu.bind(this);
	  this.navbarMenu = this.navbarMenu.bind(this);
	}

	handleMenu(data){
		this.props.pickCardset(data);
		if(this.props.history.location.pathname !== "/"){
			this.props.history.push('/');
		}
	}

	navbarMenu(){

	}

	render() {
		const {cards} = this.props.card;
		return (
			<div className="header">
				<div className="header-top">
					<div className="wrapper">
						<Link to="/"><img  className="logo-header" src={LogoWeb} srcSet={`${LogoMobile} 320w, ${LogoWeb} 1366w`} alt="Hearthstone Logo"/></Link>
						<div className="search"><SearchBar/></div>
					</div>
				</div>
				<div className="header-bottom">
					<div className="wrapper">
						<ul className="navbar">
							<li><Link to="/">Game Guides</Link></li>
							<li onClick={this.navbarMenu}>Card Sets 
									<ul className="navbar-cardset" dir="rtl">
									{
										Object.keys(cards).map((x, index) => (
												<li key={index} onClick={()=>this.handleMenu(x)}><span>{x}</span></li>
										))
									}
									</ul>
							</li>
							<li><Link to="/">Community</Link></li>
							<li><Link to="/">News</Link></li>
							<li><Link to="/">Fireside Gatherings</Link></li>
							<li><Link to="/">Esports</Link></li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  	card:state.card,
    error:state.error
});

export default withRouter(connect(mapStateToProps, { pickCardset, setCardLoading })(Header));