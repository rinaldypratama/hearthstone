import React, { Component } from 'react';
import './Jumbotron.css';

import LogoJumboWeb from "../assets/logo-full-web.png";
import LogoJumboMobile from "../assets/logo-full-mobile.png";
import IconWeb from "../assets/icon-web.png";
import IconMobile from "../assets/icon-mobile.png";

class Jumbotron extends Component {
	render() {
		return (
			<div className="jumbotron-container clearfix">
				<div className="bg-jumbotron">
						<img className="logo-jumbotron" src={LogoJumboWeb} srcSet={`${LogoJumboMobile} 320w, ${LogoJumboWeb} 1366w`} alt="Logo Hearthstone"/>
						<h1>Deceptively Simple. Insanely Fun.</h1>
				</div>
				<img className="icon-jumbotron" src={IconWeb} srcSet={`${IconMobile} 320w, ${IconWeb} 1366w`} alt=""/>
				<p>A fast-paced strategy card game for everyone.</p>
			</div>
		);
	}
}

export default Jumbotron;