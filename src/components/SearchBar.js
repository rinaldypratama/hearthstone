import React, { Component } from 'react';
import './SearchBar.css';

import SearchWeb from '../assets/search-icon-web.png';
import SearchMobile from '../assets/search-icon-mobile.png';

class SearchBar extends Component {
	constructor(props) {
	  super(props);
	  this.state = {
      show: false
    };
	  this.handleMenu = this.handleMenu.bind(this);
	}

	handleMenu(){
		this.setState({
			show: !this.state.show
		});
	}

	render() {
		const { show } = this.state;
		return (
			<div>
				<div className="search-container">
					<div className="search-bar">
						<img className="icon-search" src={SearchWeb} srcSet={`${SearchMobile} 320w, ${SearchWeb} 1366w`} alt="O"/>
	     			<input type="search" className="search-input" placeholder="search" />		
					</div>
	     		<div className="search-option" onClick={this.handleMenu}> 
	     			category
	     		</div>	
				</div>
				{	show && <div className="option" dir="rtl">
						<ul>
							<li><span>card sets</span></li>
							<li><span>guide</span></li>
							<li><span>adventures</span></li>
							<li><span>forum</span></li>
							<li><span>news</span></li>
							<li><span>heroes</span></li>
						</ul>
					</div>
				}
			</div>
		);
	}
}

export default SearchBar;