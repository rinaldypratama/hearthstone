import React, { Component } from 'react';
import { connect } from 'react-redux';
import './DetailCard.css';

import CardWrapper from '../components/CardWrapper';

import IconWeb from "../assets/icon-web.png";
import IconMobile from "../assets/icon-mobile.png";


class DetailCard extends Component {

	componentDidMount(){
		window.scrollTo(0,0);
		if(Object.keys(this.props.card.cardDetail).length === 0){
			this.props.history.push('/');
		}
	}

	render() {
		const { name, type, playerClass, rarity, cardSet, race, img } = this.props.card.cardDetail;
		const craft = rarity === "Legendary" ?
											"1600 / 3200" : rarity === "Epic" ?
											"400/ 1600" : rarity === "Rare" ?
											"100 / 800" : "40 / 400";
		const disenchant = rarity === "Legendary" ?
											"400 / 1600" : rarity === "Epic" ?
											"100/ 400" : rarity === "Rare" ?
											"20 / 100" : "5 / 50";
		return (
			<div className="wrapper">
				<div className="detailcard">
					<div className="detail-frame">
						<CardWrapper detail="true" image={img}/>
					</div>
					<div className="card-info">
						<h1>{name}</h1>
						<div className="border-info">
							<img src={IconWeb} srcSet={`${IconMobile} 320w, ${IconWeb} 1366w`} alt=""/>
						</div>
						<p><b>Type:</b> {type}</p>
						<p><b>Class:</b> {playerClass}</p>
						<p><b>Rarity:</b> {rarity}</p>
						<p><b>Set:</b> {cardSet}</p>
						<p><b>Race:</b> {race? race: "-"}</p>
						<p><b>Crafting Cost:</b> {craft} (Gold)</p>
						<p><b>Arcane Dust Gained:</b> {disenchant} (Gold)</p>
					</div>
				</div>
			</div>
		);
	}
}


const mapStateToProps = state => ({
  	card:state.card,
    error:state.error
});

export default connect(mapStateToProps, null) (DetailCard);