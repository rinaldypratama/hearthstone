import React, { Component } from 'react';
import './Home.css';

import Jumbo from '../components/Jumbotron';
import CardList from '../components/CardList';
import Footer from '../components/Footer';

class Home extends Component {
	render() {
		return (
			<div className="home">
				<Jumbo />
				<CardList />
				<Footer />
			</div>
		);
	}
}

export default Home;