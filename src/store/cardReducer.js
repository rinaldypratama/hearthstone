import {  GET_CARD, CARD_LOADING, SET_DETAILCARD, ADD_VIEW, SET_VIEW, SHOW_CARD, PICK_CARDSET  } from '../actions/types';

const initialState = {
	cards: {},
	cardShow: [],
	cardDetail: {},
	cardSet: '',
	view: null,
	addCard: null,
	loading: false
};

export default function(state = initialState, action){
	switch(action.type){
		case CARD_LOADING:
			return {
				...state,
    		loading: true
			};
		case GET_CARD:
	      return {
	        ...state,
	        cards: action.payload,
	      };
	      case SET_DETAILCARD:
	      return {
	        ...state,
	        cardDetail: action.payload
	      };
	      case ADD_VIEW:
	      return {
	        ...state,
	        view: action.payload
	      };
	      case SET_VIEW:
	      return {
	        ...state,
	        view: action.payload.view,
	        addCard: action.payload.add
	      };
	      case PICK_CARDSET:
	      return {
	        ...state,
	        cardSet: action.payload
	      };
	      case SHOW_CARD:
	      return {
	      	...state,
	      	cardShow: action.payload,
	        loading:false
	      }
		default:
			return state;
	}
}