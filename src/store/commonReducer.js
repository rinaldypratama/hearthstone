import { ADD_VIEW, SET_VIEW } from '../actions/types';

const initialState = {
	view: null,
	addCard: null,
};

export default function(state = initialState, action){
	switch(action.type){
		case ADD_VIEW:
	    return {
	      ...state,
	      view: action.payload
	    };
	    case SET_VIEW:
	    return {
	      ...state,
	      view: action.payload.view,
	      addCard: action.payload.add
	    };
		default:
			return state;
	}
}