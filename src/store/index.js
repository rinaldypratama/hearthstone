import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import cardReducer from './cardReducer';
import errorReducer from './errorReducer';
import commoneducer from './commonReducer';


const rootReducer = combineReducers({
	card: cardReducer,
	common: commoneducer,
	error: errorReducer
});

const initialState = {};

const middleware = [thunk];
// eslint-disable-next-line
const composeMiddleware = compose(
	applyMiddleware(...middleware)
	// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
	rootReducer,
	initialState,
	// applyMiddleware(...middleware)
	composeMiddleware
);

export default store;