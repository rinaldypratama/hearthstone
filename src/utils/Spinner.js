import React from 'react';
import spinner from '../assets/icon-mobile.png';
import './Spinner.css';

export default () => {
  return (
    <div className="spinner-wrapper">
      <img
      	className="spinner"
        src={spinner}
        alt="Loading..."
      />
      <p>Loading...</p>
    </div>
  );
};
